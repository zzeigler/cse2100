import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BOARD)

GPIO.setup(12,GPIO.OUT)
GPIO.output(12,1)
GPIO.setup(16,GPIO.OUT)
GPIO.output(16,1)
GPIO.setup(21,GPIO.OUT)
GPIO.output(21,1)

try:
	while(True):
		request = raw_input("Input RGB on or off: ")
		if(len(request) == 3):
			GPIO.output(12,int(request[0]))
			GPIO.output(16,int(request[1]))
			GPIO.output(21,int(request[2]))

except KeyboardInterrupt:
	print("")
	GPIO.cleanup()
