#!/bin/bash

## Disable echo used for error detection
## Not used for this script
## echo is used by rPi for led toggle
# echo() { :; }

##################
## Default vars ##
##################
n=1   # loop counter
max=0   # man number of times to repeat
unit=.250   # unit of time for morse '.250' is default
pointer='^'   # keeps track of what is printing

# this is timing for long
lUnit=$(echo - | awk -v unit="$unit" '{print unit * 3}')
# this is the timing for space (number shown +1)
sUnit=$(echo - | awk -v unit="$unit" '{print unit * 6}')
# this is the timing between repeat (number shown +1)
rUnit=$(echo - | awk -v unit="$unit" '{print unit * 13}')

clear
echo '###############################################'
echo '## What would you like to see in morse code? ##'
echo '###############################################'

# takes in code to flash
read -p 'Sentence: ' mcSet

# takes in times for code to flash
echo ''
read -p 'How many times should it flash: ' max

## Setup ##
# Turn off rPi leds
echo 0 | sudo tee /sys/class/leds/led0/brightness
echo 0 | sudo tee /sys/class/leds/led1/brightness
clear

echo Thanks, flashing \'$mcSet\' $max times now.

sleep 3

################################
## Functions for code         ##
################################
# Print dialog
oPrint () {
	clear
	echo $mcSet
	echo $pointer
	echo 

}

# Short signal
short () {
	echo 1 | sudo tee /sys/class/leds/led1/brightness
	sleep $unit
	echo 0 | sudo tee /sys/class/leds/led1/brightness
	sleep $unit

}

# Long signal
long () {
	echo 1 | sudo tee /sys/class/leds/led1/brightness
	sleep $lUnit
	echo 0 | sudo tee /sys/class/leds/led1/brightness
	sleep $unit

}

# End of letter 3 units of time between letters (2 plus 1 signal)
end () {
	sleep $unit
	sleep $unit

}

# Send morse code for A - ---
mcA () {
	echo Short - Long
	short
	long

}

# Send morse code for B --- - - -
mcB () {
	echo Long - Short - Short - Short
	long
	short
	short
	short

}

# Send morse code for C --- - --- -
mcC () {
	echo Long - Short - Long - Short
	long
	short
	long
	short

}

# Send morse code for D --- - -
mcD () {
	echo Long - Short - Short
	long
	short
	short

}

# Send morse code for E -
mcE () {
	echo Short
	short

}

# Send morse code for F - - --- -
mcF () {
	echo Short - Short - Long - Short
	short
	short
	long
	short

}

# Send morse code for G --- --- -
mcG () {
	echo Long - Long - Short
	long
	long
	short

}

# Send morse code for H - - - -
mcH () {
	echo Short - Short - Short - Short
	short
	short
	short
	short

}

# Send morse code for I - -
mcI () {
	echo Short - Short
	short
	short

}

# Send morse code for J - --- --- ---
mcJ () {
	echo Short - Long - Long - Long
	short
	long
	long
	long

}

# Send morse code for K --- - ---
mcK () {
	echo Long - Short - Long
	long
	short
	long

}

# Send morse code for L - --- - -
mcL () {
	echo Short - Long - Short - Short
	short
	long
	short
	short

}

# Send morse code for M --- ---
mcM () {
	echo Long - Long
	long
	long

}

# Send morse code for N --- -
mcN () {
	echo Long - Short
	long
	short

}

# Send morse code for O --- --- ---
mcO () {
	echo Long - Long - Long
	long
	long
	long

}

# Send morse code for P - --- --- -
mcP () {
	echo Short - Long - Long - Short
	short
	long
	long
	short

}

# Send morse code for Q --- --- - ---
mcQ () {
	echo Long - Long - Short - Long
	long
	long
	short
	long

}

# Send morse code for R - --- -
mcR () {
	echo Short - Long - Short
	short
	long
	short

}

# Send morse code for S - - -
mcS () {
	echo Short - Short - Short
	short
	short
	short

}

# Send morse code for T ---
mcT () {
	echo Long
	long
	
}

# Send morse code for U - - ---
mcU () {
	echo Short - Short - Long
	short
	short
	long

}

# Send morse code for V - - - ---
mcV () {
	echo Short - Short - Short - Long
	short
	short
	short
	long

}

# Send morse code for W - --- ---
mcW () {
	echo Short - Long - Long
	short
	long
	long

}

# Send morse code for X --- - - ---
mcX () {
	echo Long - Short - Short - Long
	long
	short
	short
	long

}

# Send morse code for Y --- - --- ---
mcY () {
	echo Long - Short - Long - Long
	long
	short
	long
	long

}

# Send morse code for Z --- --- - -
mcZ () {
	echo Long - Long - Short - Short
	long
	long
	short
	short

}

# Send morse code for 0 --- --- --- --- ---
mc0 () {
	long
	long
	long
	long
	long

}

# Send morse code for 1 - --- --- --- ---
mc1 () {
	short
	long
	long
	long
	long

}

# Send morse code for 2 - - --- --- ---
mc2 () {
	short
	short
	long
	long
	long

}

# Send morse code for 3 - - - --- ---
mc3 () {
	short
	short
	short
	long
	long

}

# Send morse code for 4 - - - - ---
mc4 () {
	short
	short
	short
	short
	long
	
}

# Send morse code for 5 - - - - -
mc5 () {
	short
	short
	short
	short
	short
	
}

# Send morse code for 6 --- - - - -
mc6 () {
	long
	short
	short
	short
	short

}

# Send morse code for 7 --- --- - - -
mc7 () {
	long
	long
	short
	short
	short

}

# Send morse code for 8 --- --- --- - -
mc8 () {
	long
	long
	long
	short
	short

}

# Send morse code for 9 --- --- --- --- -
mc9 () {
	long
	long
	long
	long
	short

}


################################
## Switch case for letters    ##
################################
switch () {
	case "$C" in
	[Aa])
		mcA
		end
		;;
	[Bb])
		mcB
		end
		;;
	[Cc])
		mcC
		end
		;;
	[Dd])
		mcD
		end
		;;
	[Ee])
		mcE
		end
		;;
	[Ff])
		mcF
		end
		;;
	[Gg])
		mcG
		end
		;;
	[Hh])
		mcH
		end
		;;
	[Ii])
		mcI
		end
		;;
	[Jj])
		mcJ
		end
		;;
	[Kk])
		mcK
		end
		;;
	[Ll])
		mcL
		end
		;;
	[Mm])
		mcM
		end
		;;
	[Nn])
		mcN
		end
		;;
	[Oo])
		mcO
		end
		;;
	[Pp])
		mcP
		end
		;;
	[Qq])
		mcQ
		end
		;;
	[Rr])
		mcR
		end
		;;
	[Ss])
		mcS
		end
		;;
	[Tt])
		mcT
		end
		;;
	[Uu])
		mcU
		end
		;;
	[Vv])
		mcV
		end
		;;
	[Ww])
		mcW
		end
		;;
	[Xx])
		mcX
		end
		;;
	[Yy])
		mcY
		end
		;;
	[Zz])
		mcZ
		end
		;;
	"0")
		mc0
		end
		;;
	"1")
		mc1
		end
		;;
	"2")
		mc2
		end
		;;
	"3")
		mc3
		end
		;;
	"4")
		mc4
		end
		;;
	"5")
		mc5
		end
		;;
	"6")
		mc6
		end
		;;
	"7")
		mc7
		end
		;;
	"8")
		mc8
		end
		;;
	"9")
		mc9
		end
		;;
	" ")
		echo Delay for next word
		# space between words 7 units.  1 unit is .250
		sleep $sUnit
		;;
	*)
		# echo Character \'$C\' not morse code, skipping.
		;;
	esac
}


################################
## This is the main file loop ##
################################
while (( $n <= $max ))
do
	for (( i=0; i<${#mcSet}; i++))
	do
		C="${mcSet:$i:1}"
		oPrint
		switch   # Sends letter to flash switch case

		# Moves pointer
		pointer=$(echo - | echo "${pointer//^/-^}")
	done

	clear
	pointer='^'
	echo Displayed Morse code of \'$mcSet\' $n times of $max.

	sleep $rUnit
	n=$(( n+1 ))
done

echo 1 | sudo tee /sys/class/leds/led0/brightness
echo 1 | sudo tee /sys/class/leds/led1/brightness
echo mmc0 | sudo tee /sys/class/leds/led0/trigger
echo input | sudo tee /sys/class/leds/led1/trigger
clear
echo Finished displaying \'$mcSet\' $max times.
echo Raspberry Pi lights have been reset to default.
