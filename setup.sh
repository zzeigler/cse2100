sudo apt-get -y install git cmake cutecom tree glade pkg-config autokey-gtk doxygen &&

PWD=$(pwd)

echo "Go to https://www.arduino.cc/en/Main/OldSoftwareReleases#previous and download v6.11"
cd ~/Downloads
wget https://downloads.arduino.cc/arduino-1.6.11-linuxarm.tar.xz &&
tar -xf arduino-1.6.11-linuxarm.tar.xz &&
cd arduino-1.6.11 &&
sudo ./install.sh  &&
echo "$ sudo ./install.sh"
echo "Then $gitclone/cse2100/teensy/install_teensyduino.sh"
echo "Select install dir for arduino software"
read -p "Press enter to continue"
# read -n 1 -s -r -p "Press any key to continue"
